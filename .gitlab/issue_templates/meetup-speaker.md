# Meetup Speaker Support Template

<!--
Note: this template is intended for people who will be speaking at events.  If you plan to both be a speaker and an organizer for an event, please use our [meetup-both](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-both) template. 
-->

## Event Info

Please include the following information about your event: 

- Event/Group Name: 
- Event/Group URL: 
- Organizer Name: 
- Location: 
- Date: 
- Time: 
- Speakers: 
- Description: 

## Speaker Info

- Speaker Name: 
- Talk Title: 
- Abstract: 
- Bio: 
- Notes:
- Link to headshot (optional but encouraged):
- Twitter name (optional but encouraged):  


## TODO for Speaker 

- [ ] Update speaker and event information in meetup speaker issue. 
- [ ] Confirm venue AV capabilities. Does the venue have wifi? Will you have a microphone? How large is the screen (small screens may reduce visibility of small text)? Will you be using your own laptop? What type of cable does the venue supply? Will you need an adapter? Should you provide your own or does the venue provide? 
- [ ] Share link to slides or talking points as a comment in this issue. Always helps to include `@your_twitter` and [@gitlab](https://twitter.com/gitlab) in the footer of your slides so folks can tweet to you about your talk. 
- [ ] Schedule a practice session for your talk with colleagues and invite @johncoghlan. It is best practice to mimic the room setup as much as possible so feel free to stand up during your presentation, etc. 
- [ ] Submit expenses associated with the event. 
- [ ] Share your slides and a link to our [meetups feedback form](https://docs.google.com/forms/d/167QK2Oudqrdu_hpRCK45G7WDW65IDV63XWgjKa6Srrw/edit) in the event discussion on Meetup after the event. 

## TODO for Evangelist Program Manager 

- [ ] Add to Meetup tracker and [GitLab Events](https://about.gitlab.com/events/) page.  
- [ ] Order swag (usually stickers and 3 shirts) and ship to speaker, event organizer, or venue. Confirm shipping instructions and audience size before sending. 
- [ ] Review slides.
- [ ] Give feedback on practice session. 
- [ ] Submit [issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) to tweet about the event. 
- [ ] Confirm the number of attendees. 
- [ ] Process expenses. 

/label ~Meetups ~"Evangelist Program"  ~"status:plan" 
/assign @johncoghlan
/due
