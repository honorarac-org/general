# GitLab Evangelist Program 

## Overview 

At GitLab our mission is to change all creative work from read-only to read-write so that everyone can contribute. In order to fulfill this mission, we need to create both the tools and platform to enable this change and a community of contributors who share our mission. We are just getting started in building the GitLab community and we invite you to join.

There are many ways to participate in the GitLab community today: contributing to an open source project, hosting your open source project on GitLab, or teaching your colleagues and collaborators about the value of Concurrent DevOps.

Our evangelist program supports people who share our mission and want to give tech talks, run local meetups, or create videos or blogs. Please [open an issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues) on this project or email 'evangelists@gitlab.com' if you have feedback on our vision, ideas for how we can build our community, or other suggestions.

## Evangelist Program Handbook

The [Evangelist Program page](https://about.gitlab.com/handbook/marketing/community-relations/evangelist-program/) in our GitLab Handbook contains important information.

## Get Involved

Please visit our [Evangelist Program page](https://about.gitlab.com/community/evangelists) to learn more about how you can get involved. 
